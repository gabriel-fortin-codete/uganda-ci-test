package co.codete.android.uganda;

import android.app.Activity;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by gabriel on 08.07.15.
 */


@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class)

public class ProstyTestRobolectric {

	@Test(expected = RuntimeException.class)
	public void test_1() {
		throw new RuntimeException("GOOD, ten test ma się wywalać");
	}

	@Test
	public void test_2() {
		// Test przechodzący zawsze
	}

	@Test
	public void napis_sprawdzTekst() {
		Activity activity = Robolectric.setupActivity(MainActivity.class);
		activity.findViewById(R.id.przycisk).performClick();
		TextView napis = (TextView) activity.findViewById(R.id.napis);

		assertThat("Czy napis się zmienił",
				napis.getText(),
				is(equalTo((CharSequence)"Monachomachia")));
//				is(equalTo("byle co")));
	}
}
